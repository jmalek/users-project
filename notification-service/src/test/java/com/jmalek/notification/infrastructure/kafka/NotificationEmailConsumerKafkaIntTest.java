package com.jmalek.notification.infrastructure.kafka;

import com.jmalek.notification.application.EmailService;
import com.jmalek.notification.config.NotificationEmailKafkaConfig;
import com.jmalek.notification.config.NotificationEmailKafkaTestConfig;
import com.jmalek.notification.config.props.NotificationKafkaProps;
import com.jmalek.users.domain.model.email.SuccessfulUserRegistrationEvent;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@ActiveProfiles("notification_email_kafka_test")
@DirtiesContext
@EmbeddedKafka(
        partitions = 1,
        brokerProperties = "listeners=PLAINTEXT://localhost:19092",
        topics = "${kafka-topics.user.registration.email.consumer}"
)
@EnableTransactionManagement
@SpringBootTest(classes = NotificationEmailConsumerKafka.class)
@Import({KafkaAutoConfiguration.class, NotificationEmailKafkaConfig.class, NotificationEmailKafkaTestConfig.class, NotificationEmailProducerKafkaMock.class})
@EnableConfigurationProperties(value = NotificationKafkaProps.class)
class NotificationEmailConsumerKafkaIntTest {

  @Autowired
  private NotificationEmailProducerKafkaMock emailProducerKafkaMock;
  @MockBean
  private EmailService emailService;

  @Test
  public void sendSuccessfulRegistrationEmail() {
    //given
    String userId = "userid1";
    String email = "aaaaa@bbbb.eu";
    String emailSubject = "subject1";
    String emailText = "text1";
    //when
    SuccessfulUserRegistrationEvent event = emailProducerKafkaMock.sendSuccessfulUserRegistration(userId, email, emailSubject, emailText);
    //then
    assertThat(event.getUserId()).isEqualTo(userId);
    assertThat(event.getEmailTo()).isEqualTo(email);
    await().untilAsserted(() -> verify(emailService, times(1)).send(email, emailSubject, emailText));
  }

}
package com.jmalek.notification.application.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class EmailServiceImplTest {

  private EmailServiceImpl sut;
  private JavaMailSender javaMailSenderMock;

  @BeforeEach
  void setUp() {
    javaMailSenderMock = mock(JavaMailSender.class);
    sut = new EmailServiceImpl(javaMailSenderMock);
  }

  @Test
  void shouldSendEmail() {
    //given
    String email = "emailTo1";
    String emailSubject = "emailSubject1";
    String emailText = "emailText1";
    //when
    sut.send(email, emailSubject, emailText);
    //then
    verify(javaMailSenderMock, times(1)).send(simpleMailMessage(email, emailSubject, emailText));
  }

  private SimpleMailMessage simpleMailMessage(String emailTo, String emailSubject, String emailText) {
    var simpleMailMessage = new SimpleMailMessage();
    simpleMailMessage.setFrom("noreply@notifications-service.eu");
    simpleMailMessage.setTo(emailTo);
    simpleMailMessage.setSubject(emailSubject);
    simpleMailMessage.setText(emailText);
    return simpleMailMessage;
  }
}
package com.jmalek.notification.infrastructure.kafka;

import com.jmalek.notification.application.EmailService;
import com.jmalek.users.domain.model.email.SuccessfulUserRegistrationEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "kafka-topics.user.registration.email", name = "consumer")
public class NotificationEmailConsumerKafka {

  private EmailService emailService;

  public NotificationEmailConsumerKafka(EmailService emailService) {
    this.emailService = emailService;
  }

  @KafkaListener(
          topics = "${kafka-topics.user.registration.email.consumer}",
          containerFactory = "serviceKafkaListenerContainerFactory",
          groupId = "users_email.SuccessfulUserRegistrationEvent")
  public void listen(@Payload SuccessfulUserRegistrationEvent event) {
    emailService.send(event.getEmailTo(), event.getEmailSubject(), event.getEmailText());
  }

}

package com.jmalek.notification.config;

import com.jmalek.notification.config.props.NotificationKafkaProps;
import com.jmalek.users.domain.model.email.SuccessfulUserRegistrationEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.RetryListener;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class NotificationEmailKafkaConfig {

  @Autowired
  NotificationKafkaProps kafkaProps;

  public ConsumerFactory<String, SuccessfulUserRegistrationEvent> consumerFactory() {
    Map<String, Object> consumerConfigs = new HashMap<>();
    consumerConfigs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProps.getBootstrapServers());
    consumerConfigs.put(ConsumerConfig.GROUP_ID_CONFIG, "users_email");
    consumerConfigs.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    consumerConfigs.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    consumerConfigs.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
    return new DefaultKafkaConsumerFactory<>(
            consumerConfigs,
            new StringDeserializer(),
            new JsonDeserializer<>(SuccessfulUserRegistrationEvent.class)
    );
  }

  @Bean
  public ConcurrentKafkaListenerContainerFactory<String, SuccessfulUserRegistrationEvent> serviceKafkaListenerContainerFactory() {
    ConcurrentKafkaListenerContainerFactory<String, SuccessfulUserRegistrationEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(consumerFactory());
    factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.RECORD);
    factory.setRetryTemplate(new RetryTemplate() {{
      setRetryPolicy(new SimpleRetryPolicy(3));
      setBackOffPolicy(new ExponentialBackOffPolicy() {{
        setInitialInterval(500);
      }});
      setListeners(new RetryListener[]{new RetryListenerSupport()});
    }});
    return factory;
  }

  @Slf4j
  static class RetryListenerSupport extends org.springframework.retry.listener.RetryListenerSupport {

    @Override
    public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
      ConsumerRecord record = (ConsumerRecord) context.getAttribute("record");
      log.info("Retry " + context.getRetryCount() + " " + record.key() + " " + record.topic() + " " + record.offset() + " because of", throwable);
    }
  }
}

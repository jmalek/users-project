package com.jmalek.notification.application;

public interface EmailService {
  void send(String emailTo, String emailSubject, String emailText);
}

package com.jmalek.notification.application.impl;

import com.jmalek.notification.application.EmailService;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

  private JavaMailSender javaMailSender;

  public EmailServiceImpl(JavaMailSender javaMailSender) {
    this.javaMailSender = javaMailSender;
  }

  @Override
  public void send(String emailTo, String emailSubject, String emailText) {
    SimpleMailMessage simpleMailMessage = createSimpleMailMessage(emailTo, emailSubject, emailText);
    javaMailSender.send(simpleMailMessage);
  }

  private SimpleMailMessage createSimpleMailMessage(String emailTo, String emailSubject, String emailText) {
    var simpleMailMessage = new SimpleMailMessage();
    simpleMailMessage.setFrom("noreply@notifications-service.eu");
    simpleMailMessage.setTo(emailTo);
    simpleMailMessage.setSubject(emailSubject);
    simpleMailMessage.setText(emailText);
    return simpleMailMessage;
  }
}

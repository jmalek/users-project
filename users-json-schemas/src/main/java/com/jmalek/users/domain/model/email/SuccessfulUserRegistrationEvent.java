package com.jmalek.users.domain.model.email;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class SuccessfulUserRegistrationEvent {

  private String userId;
  private String emailTo;
  private String emailSubject;
  private String emailText;

  public SuccessfulUserRegistrationEvent() {
  }

  public SuccessfulUserRegistrationEvent(String userId, String emailTo, String emailSubject, String emailText) {
    this.userId = userId;
    this.emailTo = emailTo;
    this.emailSubject = emailSubject;
    this.emailText = emailText;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getEmailTo() {
    return emailTo;
  }

  public void setEmailTo(String emailTo) {
    this.emailTo = emailTo;
  }

  public String getEmailSubject() {
    return emailSubject;
  }

  public void setEmailSubject(String emailSubject) {
    this.emailSubject = emailSubject;
  }

  public String getEmailText() {
    return emailText;
  }

  public void setEmailText(String emailText) {
    this.emailText = emailText;
  }
}

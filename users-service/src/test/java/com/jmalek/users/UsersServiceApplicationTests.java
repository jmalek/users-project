package com.jmalek.users;

import com.jmalek.users.config.UserServiceTestConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

@SpringBootTest
@Import(UserServiceTestConfig.class)
class UsersServiceApplicationTests {

  @Test
  void contextLoads() {
  }

}

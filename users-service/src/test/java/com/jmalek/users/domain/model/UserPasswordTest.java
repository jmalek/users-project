package com.jmalek.users.domain.model;


import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;


class UserPasswordTest {

  private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

  @Test
  void passwordShouldMatch() {
    //given
    UserPassword userPassword = new UserPassword("passwordUser1", passwordEncoder);
    //when
    boolean result = passwordEncoder.matches("passwordUser1", userPassword.getValue());
    //then
    assertThat(result).isTrue();
  }

  @Test
  void passwordShouldNotMatch() {
    //given
    UserPassword userPassword = new UserPassword("passwordUser1", passwordEncoder);
    //when
    boolean result = passwordEncoder.matches("passwordUserXXXXX", userPassword.getValue());
    //then
    assertThat(result).isFalse();
  }
}
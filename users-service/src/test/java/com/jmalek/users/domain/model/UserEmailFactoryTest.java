package com.jmalek.users.domain.model;

import com.jmalek.users.domain.model.UserEmail.UserEmailFactoryImpl;
import com.jmalek.users.domain.util.TryResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.jmalek.users.domain.model.UserEmail.UserEmailFactoryImpl.EMAIL_HAS_INVALID_FORMAT;
import static com.jmalek.users.domain.model.UserEmail.UserEmailFactoryImpl.EMAIL_IS_ALREADY_IN_USE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class UserEmailFactoryTest {

  private UserEmailFactoryImpl sut;
  private UserRepository userRepository;

  @BeforeEach
  void setUp() {
    userRepository = mock(UserRepository.class);
    sut = new UserEmailFactoryImpl(userRepository);
  }

  @Test
  void emailHasGoodFormat() {
    //given
    //when
    TryResult<UserEmail> userEmailTryResult = sut.create("aaaaa@bbbb.eu");
    //then
    assertThat(userEmailTryResult.isError()).isFalse();
    assertThat(userEmailTryResult.getValue().getValue()).isEqualTo("aaaaa@bbbb.eu");
  }

  @Test
  void emailHasInvalidFormatWhenNull() {
    //given
    //when
    TryResult<UserEmail> userEmailTryResult = sut.create(null);
    //then
    assertThat(userEmailTryResult.isError()).isTrue();
    assertThat(userEmailTryResult.getErrorMessage()).isEqualTo(EMAIL_HAS_INVALID_FORMAT);
  }

  @Test
  void emailHasInvalidFormatWhenEmptyString() {
    //given
    //when
    TryResult<UserEmail> userEmailTryResult = sut.create("");
    //then
    assertThat(userEmailTryResult.isError()).isTrue();
    assertThat(userEmailTryResult.getErrorMessage()).isEqualTo(EMAIL_HAS_INVALID_FORMAT);
  }

  @Test
  void emailHasInvalidFormat() {
    //given
    //when
    TryResult<UserEmail> userEmailTryResult = sut.create("aaaaa@bbbb.zzzz");
    //then
    assertThat(userEmailTryResult.isError()).isTrue();
    assertThat(userEmailTryResult.getErrorMessage()).isEqualTo(EMAIL_HAS_INVALID_FORMAT);
  }

  @Test
  void emailIsAlreadyInUse() {
    //given
    given(userRepository.checkIfEmailExists(new UserEmail("aaaaa@bbbb.eu"))).willReturn(true);
    //when
    TryResult<UserEmail> userEmailTryResult = sut.create("aaaaa@bbbb.eu");
    //then
    assertThat(userEmailTryResult.isError()).isTrue();
    assertThat(userEmailTryResult.getErrorMessage()).isEqualTo(EMAIL_IS_ALREADY_IN_USE);
  }

}
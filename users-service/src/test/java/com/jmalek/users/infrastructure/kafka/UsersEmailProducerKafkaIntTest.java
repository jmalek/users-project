package com.jmalek.users.infrastructure.kafka;

import com.jmalek.users.config.UserEmailKafkaConfig;
import com.jmalek.users.config.UserEmailKafkaTestConfig;
import com.jmalek.users.config.props.KafkaProps;
import com.jmalek.users.domain.model.UserEmail;
import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.model.email.SuccessfulUserRegistrationEvent;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ActiveProfiles("registration_user_email_kafka_test")
@DirtiesContext
@EmbeddedKafka(
        partitions = 1,
        brokerProperties = "listeners=PLAINTEXT://localhost:19092",
        topics = "${kafka-topics.user.registration.email.consumer}"
)
@EnableTransactionManagement
@SpringBootTest(classes = EmailConsumerMock.class)
@Import({KafkaAutoConfiguration.class, UserEmailKafkaConfig.class, UserEmailKafkaTestConfig.class, EmailProducerKafka.class})
@EnableConfigurationProperties(value = KafkaProps.class)
class UsersEmailProducerKafkaIntTest {

  @Autowired
  private EmailProducerKafka sut;

  @MockBean
  private EmailServiceMock emailServiceMock;

  @Test
  public void sendSuccessfulRegistrationEmail() {
    //given
    UserId userId = new UserId("userid1");
    UserEmail userEmailMock = mock(UserEmail.class);
    String email = "aaaaa@bbbb.eu";
    given(userEmailMock.getValue()).willReturn(email);
    //when
    SuccessfulUserRegistrationEvent event = sut.sendSuccessfulUserRegistration(userId.getValue(), userEmailMock.getValue());
    //then
    assertThat(event.getUserId()).isEqualTo(userId.getValue());
    assertThat(event.getEmailTo()).isEqualTo(email);
    await().untilAsserted(() -> verify(emailServiceMock, times(1)).send());
  }

}
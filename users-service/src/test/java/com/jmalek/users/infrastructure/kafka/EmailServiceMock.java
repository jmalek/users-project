package com.jmalek.users.infrastructure.kafka;

public interface EmailServiceMock {

  void send();
}

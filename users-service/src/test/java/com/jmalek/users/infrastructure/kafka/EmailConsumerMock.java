package com.jmalek.users.infrastructure.kafka;

import com.jmalek.users.domain.model.email.SuccessfulUserRegistrationEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "kafka-topics.user.registration.email", name = "consumer")
public class EmailConsumerMock {

  private EmailServiceMock emailServiceMock;

  public EmailConsumerMock(EmailServiceMock emailServiceMock) {
    this.emailServiceMock = emailServiceMock;
  }

  @KafkaListener(
          topics = "${kafka-topics.user.registration.email.consumer}",
          containerFactory = "serviceKafkaListenerContainerFactory",
          groupId = "users_email.SuccessfulUserRegistrationEvent")
  public void listen(@Payload SuccessfulUserRegistrationEvent event) {
    emailServiceMock.send();
  }

}

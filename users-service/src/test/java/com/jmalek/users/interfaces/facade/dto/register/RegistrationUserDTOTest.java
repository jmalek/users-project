package com.jmalek.users.interfaces.facade.dto.register;

import com.jmalek.users.domain.util.TryResult;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RegistrationUserDTOTest {

  @Test
  void validationOk() {
    //given
    RegistrationUserDTO registrationUserDTO = new RegistrationUserDTO("name", "password", "email@xxx.com");
    //when
    TryResult<String> validate = registrationUserDTO.validate();
    //then
    assertThat(validate.isError()).isFalse();
  }

  @Test
  void nameIsRequired() {
    //given
    RegistrationUserDTO registrationUserDTO = new RegistrationUserDTO(null, "password", "email@xxx.com");
    //when
    TryResult<String> validate = registrationUserDTO.validate();
    //then
    assertThat(validate.isError()).isTrue();
    assertThat(validate.getErrorMessage()).isEqualTo("name filed is required");
  }

  @Test
  void passwordIsRequired() {
    //given
    RegistrationUserDTO registrationUserDTO = new RegistrationUserDTO("name", null, "email@xxx.com");
    //when
    TryResult<String> validate = registrationUserDTO.validate();
    //then
    assertThat(validate.isError()).isTrue();
    assertThat(validate.getErrorMessage()).isEqualTo("password filed is required");
  }

  @Test
  void emailIsRequired() {
    //given
    RegistrationUserDTO registrationUserDTO = new RegistrationUserDTO("name", "password", null);
    //when
    TryResult<String> validate = registrationUserDTO.validate();
    //then
    assertThat(validate.isError()).isTrue();
    assertThat(validate.getErrorMessage()).isEqualTo("email filed is required");
  }

}
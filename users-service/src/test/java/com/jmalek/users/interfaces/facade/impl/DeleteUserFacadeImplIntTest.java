package com.jmalek.users.interfaces.facade.impl;

import com.jmalek.users.config.IntegrationTest;
import com.jmalek.users.config.TestConfig;
import com.jmalek.users.domain.model.User;
import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.model.UserRepository;
import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.delete.DeleteUsersDTO;
import com.jmalek.users.interfaces.facade.dto.delete.DeletedUserDTO;
import com.jmalek.users.interfaces.facade.dto.delete.DeletedUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserFormsDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Optional;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

@IntegrationTest
@Import({TestConfig.class})
@EnableTransactionManagement
class DeleteUserFacadeImplIntTest {

  @Autowired
  private UserFacade sut;

  @Autowired
  private UserRepository userRepository;

  @Test
  void deleteOneUser() {
    //given
    RegisteredUserDTO userDTO = registerUser();
    //when
    DeletedUsersResultDTO deletedUser = sut.delete(new DeleteUsersDTO(asList(userDTO.getUserId())));
    //then
    DeletedUserDTO deletedUserDTO = deletedUser.getUsers().get(0);
    validateDeletedUser(userDTO, deletedUserDTO);
  }

  private void validateDeletedUser(RegisteredUserDTO userDTO, DeletedUserDTO deletedUserDTO) {
    assertThat(deletedUserDTO.getUserId()).isEqualTo(userDTO.getUserId());
    assertThat(deletedUserDTO.getHttpStatus()).isEqualTo(HttpStatus.OK.value());
    Optional<User> user = userRepository.loadUser(new UserId(userDTO.getUserId()));
    assertThat(user).isNotPresent();
  }

  @Test
  void deleteOneUserAndReturnNotFoundForDeletedUser() {
    //given
    RegisteredUserDTO userDTO = registerUser();
    //when
    DeletedUsersResultDTO deletedUsers = sut.delete(new DeleteUsersDTO(asList(userDTO.getUserId(), "notExistingUserId")));
    //then
    assertThat(deletedUsers.getUsers().size()).isEqualTo(2);
    DeletedUserDTO deletedUserDTO1 = deletedUsers.getUsers().get(0);
    validateDeletedUser(userDTO, deletedUserDTO1);
    DeletedUserDTO deletedUserDTO2 = deletedUsers.getUsers().get(1);
    assertThat(deletedUserDTO2.getErrorMessage()).isNotEmpty();
    assertThat(deletedUserDTO2.getHttpStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
  }

  private RegisteredUserDTO registerUser() {
    String email = "aaaa@bbbb.eu";
    RegistrationUserFormsDTO userFormDataDTO = new RegistrationUserFormsDTO(
            singletonList(
                    new RegistrationUserDTO("name1", "password1", email))
    );
    RegisteredUserResultDTO registeredUserResult = sut.register(userFormDataDTO);
    return registeredUserResult.getUsers().get(0);
  }
}
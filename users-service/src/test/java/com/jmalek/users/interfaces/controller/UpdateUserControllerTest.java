package com.jmalek.users.interfaces.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUserDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUserFormsDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdatedUserDTO;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class UpdateUserControllerTest {

  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper mapper;

  @MockBean
  UserFacade userFacade;

  @Test
  public void updateUsers() throws Exception {
    UpdateUserFormsDTO updateUserFormsDTO = updateUserFormsDTO();
    String content = mapper.writeValueAsString(updateUserFormsDTO);
    BDDMockito.given(userFacade.update(updateUserFormsDTO))
            .willReturn(new UpdateUsersResultDTO(
                    Arrays.asList(
                            new UpdatedUserDTO("userId1"),
                            UpdatedUserDTO.error("Password can not be empty", HttpStatus.BAD_REQUEST, "userId2")
                    )
            ));

    mockMvc.perform(
            patch("/users")
                    .contentType(APPLICATION_JSON_VALUE)
                    .content(content))
            .andExpect(status().isMultiStatus())
            .andExpect(jsonPath("$.users.[0].userId", is("userId1")))
            .andExpect(jsonPath("$.users.[0].httpStatus", is(HttpStatus.OK.value())))
            .andExpect(jsonPath("$.users.[1].userId", is("userId2")))
            .andExpect(jsonPath("$.users.[1].httpStatus", is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath("$.users.[1].errorMessage", is("Password can not be empty")));
  }

  private UpdateUserFormsDTO updateUserFormsDTO() {
    return new UpdateUserFormsDTO(
            Arrays.asList(
                    new UpdateUserDTO("userId1", "name11", "password11"),
                    new UpdateUserDTO("userId2", "name12", "")));
  }


}
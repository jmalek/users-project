package com.jmalek.users.interfaces.facade.impl;

import com.jmalek.users.application.UserService;
import com.jmalek.users.application.impl.PasswordEncoderMock;
import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserFormsDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

class RegisterFacadeImplDtoValidationTest {

  private UserFacade sut;

  @BeforeEach
  void setUp() {
    UserService userServiceMock = mock(UserService.class);
    sut = new UserFacadeImpl(userServiceMock, new PasswordEncoderMock());
  }

  @Test
  void nameIsRequired() {
    //given
    RegistrationUserFormsDTO userFormDataDTO = new RegistrationUserFormsDTO(
            singletonList(
                    new RegistrationUserDTO(null, "password1", "aaaa@bbbb.eu"))
    );
    //when
    RegisteredUserResultDTO registeredUserResult = sut.register(userFormDataDTO);
    //then
    RegisteredUserDTO registeredUser = registeredUserResult.getUsers().get(0);
    assertThat(registeredUser.getHttpStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    assertThat(registeredUser.getErrorMessage()).isEqualTo("name filed is required");
  }


  @Test
  void passwordIsRequired() {
    //given
    RegistrationUserFormsDTO userFormDataDTO = new RegistrationUserFormsDTO(
            singletonList(
                    new RegistrationUserDTO("name1", "", "aaaa@bbbb.eu"))
    );
    //when
    RegisteredUserResultDTO registeredUserResult = sut.register(userFormDataDTO);
    //then
    RegisteredUserDTO registeredUser = registeredUserResult.getUsers().get(0);
    assertThat(registeredUser.getHttpStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    assertThat(registeredUser.getErrorMessage()).isEqualTo("password filed is required");
  }

  @Test
  void emailIsRequired() {
    //given
    RegistrationUserFormsDTO userFormDataDTO = new RegistrationUserFormsDTO(
            singletonList(
                    new RegistrationUserDTO("name1", "password1", null))
    );
    //when
    RegisteredUserResultDTO registeredUserResult = sut.register(userFormDataDTO);
    //then
    RegisteredUserDTO registeredUser = registeredUserResult.getUsers().get(0);
    assertThat(registeredUser.getHttpStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    assertThat(registeredUser.getErrorMessage()).isEqualTo("email filed is required");
  }

  @Test
  void shouldReturnEmptyResultForNoFroms() {
    //given
    RegistrationUserFormsDTO userFormDataDTO = new RegistrationUserFormsDTO();
    //when
    RegisteredUserResultDTO registeredUserResult = sut.register(userFormDataDTO);
    //then
    assertThat(registeredUserResult.getUsers()).isEmpty();
  }

  @Test
  void shouldReturnEmptyResultForNull() {
    //given
    //when
    RegisteredUserResultDTO registeredUserResult = sut.register(null);
    //then
    assertThat(registeredUserResult.getUsers()).isEmpty();
  }

}
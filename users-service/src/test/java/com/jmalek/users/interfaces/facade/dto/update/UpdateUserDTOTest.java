package com.jmalek.users.interfaces.facade.dto.update;

import com.jmalek.users.domain.util.TryResult;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UpdateUserDTOTest {

  public static final String BLANK = "";

  @Test
  void validationOk() {
    //given
    UpdateUserDTO updateUserDTO = new UpdateUserDTO("userId1", "name1", "password1");
    //when
    TryResult<String> validate = updateUserDTO.validate();
    //then
    assertThat(validate.isError()).isFalse();
  }

  @Test
  void validationOkForNullName() {
    //given
    UpdateUserDTO updateUserDTO = new UpdateUserDTO("userId1", null, "password1");
    //when
    TryResult<String> validate = updateUserDTO.validate();
    //then
    assertThat(validate.isError()).isFalse();
  }

  @Test
  void validationOkForNullPassword() {
    //given
    UpdateUserDTO updateUserDTO = new UpdateUserDTO("userId1", "name1", null);
    //when
    TryResult<String> validate = updateUserDTO.validate();
    //then
    assertThat(validate.isError()).isFalse();
  }

  @Test
  void errorForBlankName() {
    //given
    UpdateUserDTO updateUserDTO = new UpdateUserDTO("userId1", BLANK, "password1");
    //when
    TryResult<String> validate = updateUserDTO.validate();
    //then
    assertThat(validate.isError()).isTrue();
  }

  @Test
  void errorForBlankPassword() {
    //given
    UpdateUserDTO updateUserDTO = new UpdateUserDTO("userId1", "name1", BLANK);
    //when
    TryResult<String> validate = updateUserDTO.validate();
    //then
    assertThat(validate.isError()).isTrue();
  }

}
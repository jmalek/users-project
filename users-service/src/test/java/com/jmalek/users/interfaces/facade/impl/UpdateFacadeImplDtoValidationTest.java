package com.jmalek.users.interfaces.facade.impl;

import com.jmalek.users.application.UserService;
import com.jmalek.users.application.impl.PasswordEncoderMock;
import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUserDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUserFormsDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdatedUserDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

class UpdateFacadeImplDtoValidationTest {

  private UserFacade sut;

  @BeforeEach
  void setUp() {
    UserService userServiceMock = mock(UserService.class);
    sut = new UserFacadeImpl(userServiceMock, new PasswordEncoderMock());
  }

  @Test
  void userIdIsRequired() {
    //given
    UpdateUserFormsDTO updateUserFormsDTO = new UpdateUserFormsDTO(asList(new UpdateUserDTO(null, "name11", "password11")));
    //when
    UpdateUsersResultDTO updateUsersResultDTO = sut.update(updateUserFormsDTO);
    //then
    UpdatedUserDTO updatedUserDTO = updateUsersResultDTO.getUsers().get(0);
    assertThat(updatedUserDTO.getHttpStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    assertThat(updatedUserDTO.getErrorMessage()).isEqualTo("userId filed is required");
  }


  @Test
  void shouldReturnEmptyResultForNoUpdates() {
    //given
    UpdateUserFormsDTO updateUserFormsDTO = new UpdateUserFormsDTO();
    //when
    UpdateUsersResultDTO updateUsersResultDTO = sut.update(updateUserFormsDTO);
    //then
    assertThat(updateUsersResultDTO.getUsers()).isEmpty();
  }

  @Test
  void shouldReturnEmptyResultForNull() {
    //given
    //when
    UpdateUsersResultDTO updateUsersResultDTO = sut.update(null);
    //then
    assertThat(updateUsersResultDTO.getUsers()).isEmpty();
  }

}
package com.jmalek.users.interfaces.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.delete.DeleteUsersDTO;
import com.jmalek.users.interfaces.facade.dto.delete.DeletedUserDTO;
import com.jmalek.users.interfaces.facade.dto.delete.DeletedUsersResultDTO;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class DeleteUserControllerTest {

  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper mapper;

  @MockBean
  UserFacade userFacade;

  @Test
  public void deleteUsers() throws Exception {
    DeleteUsersDTO deleteUsersDTO = new DeleteUsersDTO(Arrays.asList("userId1", "userId2", "userId3"));
    String content = mapper.writeValueAsString(deleteUsersDTO);
    BDDMockito.given(userFacade.delete(deleteUsersDTO))
            .willReturn(new DeletedUsersResultDTO(
                    Arrays.asList(
                            new DeletedUserDTO("userId1"),
                            DeletedUserDTO.error("Not found", "userId2", HttpStatus.BAD_REQUEST),
                            new DeletedUserDTO("userId3")
                    )
            ));

    mockMvc.perform(
            delete("/users")
                    .contentType(APPLICATION_JSON_VALUE)
                    .content(content))
            .andExpect(status().isMultiStatus())
            .andExpect(jsonPath("$.users.[0].userId", is("userId1")))
            .andExpect(jsonPath("$.users.[0].httpStatus", is(HttpStatus.OK.value())))
            .andExpect(jsonPath("$.users.[1].userId", is("userId2")))
            .andExpect(jsonPath("$.users.[1].httpStatus", is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath("$.users.[2].userId", is("userId3")))
            .andExpect(jsonPath("$.users.[2].httpStatus", is(HttpStatus.OK.value())));
  }


}
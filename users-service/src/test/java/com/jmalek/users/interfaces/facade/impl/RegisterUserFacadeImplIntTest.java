package com.jmalek.users.interfaces.facade.impl;

import com.jmalek.users.config.IntegrationTest;
import com.jmalek.users.config.TestConfig;
import com.jmalek.users.domain.model.User;
import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.model.UserRepository;
import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserFormsDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Optional;

import static com.jmalek.users.domain.model.UserEmail.UserEmailFactoryImpl.EMAIL_IS_ALREADY_IN_USE;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

@IntegrationTest
@Import({TestConfig.class})
@EnableTransactionManagement
class RegisterUserFacadeImplIntTest {

  @Autowired
  private UserFacade sut;

  @Autowired
  private UserRepository userRepository;

  @Test
  void registerOneUser() {
    //given
    String email = "aaaa@bbbb.eu";
    RegistrationUserFormsDTO userFormDataDTO = new RegistrationUserFormsDTO(
            singletonList(
                    new RegistrationUserDTO("name1", "password1", email))
    );
    //when
    RegisteredUserResultDTO registeredUserResult = sut.register(userFormDataDTO);
    //then
    RegisteredUserDTO registeredUser = registeredUserResult.getUsers().get(0);
    validateSuccessfulRegisteredUser(email, registeredUser);
  }

  @Test
  void registerOneUserSuccessfullyAndOneRejectBecauseDuplicatedEmail() {
    //given
    String email = "aaaa@bbbb.eu";
    RegistrationUserFormsDTO userFormDataDTO = new RegistrationUserFormsDTO(
            asList(
                    new RegistrationUserDTO("name1", "password1", email),
                    new RegistrationUserDTO("name2", "password2", email)
            )
    );
    //when
    RegisteredUserResultDTO registeredUserResult = sut.register(userFormDataDTO);
    //then
    assertThat(registeredUserResult.getUsers().size()).isEqualTo(2);
    RegisteredUserDTO registeredUser = registeredUserResult.getUsers().get(0);
    validateSuccessfulRegisteredUser(email, registeredUser);
    RegisteredUserDTO userWithDuplicatedEmail = registeredUserResult.getUsers().get(1);
    assertThat(userWithDuplicatedEmail.getHttpStatus()).isNotEqualTo(HttpStatus.OK.value());
    assertThat(userWithDuplicatedEmail.getErrorMessage()).isEqualTo(EMAIL_IS_ALREADY_IN_USE);
  }

  private void validateSuccessfulRegisteredUser(String email, RegisteredUserDTO registeredUser) {
    assertThat(registeredUser.getHttpStatus()).isEqualTo(HttpStatus.OK.value());
    assertThat(registeredUser.getEmail()).isEqualTo(email);
    Optional<User> user = userRepository.loadUser(new UserId(registeredUser.getUserId()));
    assertThat(user).isPresent();
    assertThat(user.get().getUserEmail().getValue()).isEqualTo(email);
  }


}
package com.jmalek.users.interfaces.facade.impl;

import com.jmalek.users.config.IntegrationTest;
import com.jmalek.users.config.TestConfig;
import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.delete.DeleteUsersDTO;
import com.jmalek.users.interfaces.facade.dto.read.ReadUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.read.UserResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserFormsDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

@IntegrationTest
@Import({TestConfig.class})
@EnableTransactionManagement
class ReadUserFacadeImplIntTest {

  @Autowired
  private UserFacade sut;

  @Test
  void shouldReturnNoUsers() {
    //when
    ReadUsersResultDTO readUsersResultDTO = sut.readAll();
    //then
    assertThat(readUsersResultDTO.getUsers().size()).isEqualTo(0);
  }

  @Test
  void readAllUsers() {
    registerUser("xxxx1@yyyy.eu");
    registerUser("xxxx2@yyyy.eu");
    registerUser("xxxx3@yyyy.eu");
    registerUser("xxxx4@yyyy.eu");
    //when
    ReadUsersResultDTO readUsersResultDTO = sut.readAll();
    //then
    assertThat(readUsersResultDTO.getUsers().size()).isEqualTo(4);
  }

  @Test
  void shouldNotReadDeletedUsers() {
    RegisteredUserDTO userDTO = registerUser("xxxx1@yyyy.eu");
    registerUser("xxxx2@yyyy.eu");
    //when
    sut.delete(new DeleteUsersDTO(asList(userDTO.getUserId())));
    ReadUsersResultDTO readUsersResultDTO = sut.readAll();
    //then
    assertThat(readUsersResultDTO.getUsers().size()).isEqualTo(1);
    List<UserResultDTO> users = readUsersResultDTO.getUsers();
    assertThat(users.get(0).getEmail()).isEqualTo("xxxx2@yyyy.eu");
  }

  private RegisteredUserDTO registerUser(String email) {
    RegistrationUserFormsDTO userFormDataDTO = new RegistrationUserFormsDTO(
            singletonList(
                    new RegistrationUserDTO("name1", "password1", email))
    );
    RegisteredUserResultDTO registeredUserResult = sut.register(userFormDataDTO);
    return registeredUserResult.getUsers().get(0);
  }

}
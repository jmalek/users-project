package com.jmalek.users.interfaces.facade.impl;

import com.jmalek.users.config.IntegrationTest;
import com.jmalek.users.config.TestConfig;
import com.jmalek.users.domain.model.User;
import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.model.UserRepository;
import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserFormsDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUserDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUserFormsDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdatedUserDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

@IntegrationTest
@Import({TestConfig.class})
@EnableTransactionManagement
class UpdateUserFacadeImplIntTest {

  @Autowired
  private UserFacade sut;

  @Autowired
  private UserRepository userRepository;

  @Test
  void updateNameAndPasswordUser() {
    //given
    RegisteredUserDTO userDTO = registerUser("xxxxx@yyyy.eu", "name1", "password1");
    UpdateUserFormsDTO updateUserFormsDTO = new UpdateUserFormsDTO(asList(new UpdateUserDTO(userDTO.getUserId(), "name11", "password11")));
    //when
    UpdateUsersResultDTO update = sut.update(updateUserFormsDTO);
    //then
    UpdatedUserDTO updatedUserDTO = update.getUsers().get(0);
    assertThat(updatedUserDTO.getUserId()).isEqualTo(userDTO.getUserId());
    assertThat(updatedUserDTO.getHttpStatus()).isEqualTo(HttpStatus.OK.value());

    User user = userRepository.loadUser(new UserId(userDTO.getUserId())).get();
    assertThat(user.getUserName().getValue()).isEqualTo("name11");
    assertThat(user.getUserPassword().getValue()).isEqualTo("password11");
  }

  @Test
  void updateOnlyName() {
    //given
    RegisteredUserDTO userDTO = registerUser("xxxxx@yyyy.eu", "name1", "password1");
    UpdateUserFormsDTO updateUserFormsDTO = new UpdateUserFormsDTO(asList(new UpdateUserDTO(userDTO.getUserId(), "name11", null)));
    //when
    UpdateUsersResultDTO update = sut.update(updateUserFormsDTO);
    //then
    UpdatedUserDTO updatedUserDTO = update.getUsers().get(0);
    assertThat(updatedUserDTO.getUserId()).isEqualTo(userDTO.getUserId());
    assertThat(updatedUserDTO.getHttpStatus()).isEqualTo(HttpStatus.OK.value());

    User user = userRepository.loadUser(new UserId(userDTO.getUserId())).get();
    assertThat(user.getUserName().getValue()).isEqualTo("name11");
    assertThat(user.getUserPassword().getValue()).isEqualTo("password1");
  }

  private RegisteredUserDTO registerUser(String email, String name, String password) {
    RegistrationUserFormsDTO userFormDataDTO = new RegistrationUserFormsDTO(
            singletonList(
                    new RegistrationUserDTO(name, password, email))
    );
    RegisteredUserResultDTO registeredUserResult = sut.register(userFormDataDTO);
    return registeredUserResult.getUsers().get(0);
  }

}
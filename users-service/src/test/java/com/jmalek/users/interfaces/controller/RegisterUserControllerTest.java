package com.jmalek.users.interfaces.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserFormsDTO;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class RegisterUserControllerTest {


  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper mapper;

  @MockBean
  UserFacade userFacade;

  @Test
  public void registerUsers() throws Exception {
    RegistrationUserFormsDTO registrationUserFormDataDTO = registrationUserFormDataDTO();
    String content = mapper.writeValueAsString(registrationUserFormDataDTO);
    BDDMockito.given(userFacade.register(registrationUserFormDataDTO))
            .willReturn(new RegisteredUserResultDTO(
                    Arrays.asList(
                            new RegisteredUserDTO("xxxx1@yyyy.eu", "userId1"),
                            RegisteredUserDTO.error("Duplicated email", HttpStatus.BAD_REQUEST)
                    )
            ));

    mockMvc.perform(
            post("/users")
                    .contentType(APPLICATION_JSON_VALUE)
                    .content(content))
            .andExpect(status().isMultiStatus())
            .andExpect(jsonPath("$.users.[0].userId", is("userId1")))
            .andExpect(jsonPath("$.users.[0].email", is("xxxx1@yyyy.eu")))
            .andExpect(jsonPath("$.users.[0].httpStatus", is(HttpStatus.OK.value())))
            .andExpect(jsonPath("$.users.[1].httpStatus", is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath("$.users.[1].errorMessage", is("Duplicated email")));
  }

  private RegistrationUserFormsDTO registrationUserFormDataDTO() {
    return new RegistrationUserFormsDTO(
            Arrays.asList(
                    new RegistrationUserDTO("name1", "password1", "xxxx1@yyyy.eu"),
                    new RegistrationUserDTO("name1", "password2", "xxxx1@yyyy.eu")));
  }


}
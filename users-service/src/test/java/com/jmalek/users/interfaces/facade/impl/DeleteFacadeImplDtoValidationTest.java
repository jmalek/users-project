package com.jmalek.users.interfaces.facade.impl;

import com.jmalek.users.application.UserService;
import com.jmalek.users.application.impl.PasswordEncoderMock;
import com.jmalek.users.domain.model.User;
import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.util.TryResult;
import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.delete.DeleteUsersDTO;
import com.jmalek.users.interfaces.facade.dto.delete.DeletedUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserFormsDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DeleteFacadeImplDtoValidationTest {

  private UserFacade sut;
  private UserService userServiceMock;

  @BeforeEach
  void setUp() {
    userServiceMock = mock(UserService.class);
    sut = new UserFacadeImpl(userServiceMock, new PasswordEncoderMock());
  }

  @Test
  void omitBlankIds() {
    //given
    UserId userId1 = new UserId("userId1");
    User mockUser1 = mock(User.class);
    given(mockUser1.getUserId()).willReturn(userId1);
    given(userServiceMock.deleteUsers(userId1)).willReturn(TryResult.of(mockUser1));
    UserId userId2 = new UserId("userId2");
    User mockUser2 = mock(User.class);
    given(mockUser2.getUserId()).willReturn(userId2);
    given(userServiceMock.deleteUsers(userId2)).willReturn(TryResult.of(mockUser2));
    //when
    DeletedUsersResultDTO deletedUser = sut.delete(new DeleteUsersDTO(asList("userId1", null, "userId2", "")));
    //then
    assertThat(deletedUser.getUsers().size()).isEqualTo(2);
  }

  @Test
  void shouldReturnEmptyResultForNoIds() {
    //given
    DeleteUsersDTO deleteUsersDTO = new DeleteUsersDTO();
    //when
    DeletedUsersResultDTO delete = sut.delete(deleteUsersDTO);
    //then
    assertThat(delete.getUsers()).isEmpty();
  }

  @Test
  void shouldReturnEmptyResultForNull() {
    //given
    //when
    DeletedUsersResultDTO delete = sut.delete(null);
    //then
    assertThat(delete.getUsers()).isEmpty();
  }


}
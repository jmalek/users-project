package com.jmalek.users.interfaces.controller;

import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.read.ReadUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.read.UserResultDTO;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class ReadUserControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  UserFacade userFacade;

  @Test
  public void readAllUsers() throws Exception {
    BDDMockito.given(userFacade.readAll())
            .willReturn(new ReadUsersResultDTO(
                    Arrays.asList(
                            new UserResultDTO("userId1", "name1", "xxxx1@yyyy.eu"),
                            new UserResultDTO("userId2", "name2", "xxxx2@yyyy.eu")
                    )
            ));

    mockMvc.perform(
            get("/users")
                    .contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.users.[0].userId", is("userId1")))
            .andExpect(jsonPath("$.users.[0].email", is("xxxx1@yyyy.eu")))
            .andExpect(jsonPath("$.users.[0].name", is("name1")))
            .andExpect(jsonPath("$.users.[1].userId", is("userId2")))
            .andExpect(jsonPath("$.users.[1].email", is("xxxx2@yyyy.eu")))
            .andExpect(jsonPath("$.users.[1].name", is("name2")));
  }
}
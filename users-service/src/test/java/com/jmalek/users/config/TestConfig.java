package com.jmalek.users.config;

import com.jmalek.users.application.impl.EmailProducerMock;
import com.jmalek.users.application.impl.PasswordEncoderMock;
import com.jmalek.users.domain.model.UserIdFactory;
import com.jmalek.users.domain.model.email.EmailProducer;
import com.jmalek.users.infrastructure.factory.UserIdFactoryImpl;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

@TestConfiguration
public class TestConfig {

  @Bean
  UserIdFactory userIdFactory() {
    return new UserIdFactoryImpl();
  }

  @Bean
  EmailProducer emailProducer(){
    return new EmailProducerMock();
  }

  @Bean
  PasswordEncoder passwordEncoder(){
    return new PasswordEncoderMock();
  }



}

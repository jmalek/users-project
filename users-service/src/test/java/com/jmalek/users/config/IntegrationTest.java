package com.jmalek.users.config;

import com.jmalek.users.config.props.UsersDBProperties;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.support.EntityManagerBeanDefinitionRegistrarPostProcessor;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(TYPE)
@Retention(RUNTIME)
@ExtendWith(SpringExtension.class)
@EntityScan(basePackages = "com.jmalek.users.interfaces.domain")
@ComponentScan(basePackages = {
        "com.jmalek.users.interfaces.facade",
        "com.jmalek.users.application",
        "com.jmalek.users.infrastructure.hibernate",
        "com.jmalek.users.domain.model",
})
@Import({
        DatasourceConfig.class,
        UsersDBProperties.class,
        HibernateJpaAutoConfiguration.class,
        EntityManagerBeanDefinitionRegistrarPostProcessor.class})
@TestPropertySource(locations = "classpath:application-int_test.properties")
//@Sql(value = {"/drop.sql", "/init.sql"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public @interface IntegrationTest {
}



package com.jmalek.users.config;

import com.jmalek.users.application.impl.EmailProducerMock;
import com.jmalek.users.domain.model.email.EmailProducer;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class UserServiceTestConfig {

  @Bean
  EmailProducer emailProducer() {
    return new EmailProducerMock();
  }
}

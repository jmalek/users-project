package com.jmalek.users.application.impl;

import com.jmalek.users.domain.model.email.EmailProducer;
import com.jmalek.users.domain.model.email.SuccessfulUserRegistrationEvent;

public class EmailProducerMock implements EmailProducer {
  public static final String SUBJECT = "subject";
  public static final String TEXT = "text";
  private SuccessfulUserRegistrationEvent last;

  @Override
  public SuccessfulUserRegistrationEvent sendSuccessfulUserRegistration(String userId, String userEmail) {
    this.last = new SuccessfulUserRegistrationEvent(userId, userEmail, SUBJECT, TEXT);
    return this.last;
  }

  public SuccessfulUserRegistrationEvent getLast() {
    return last;
  }
}

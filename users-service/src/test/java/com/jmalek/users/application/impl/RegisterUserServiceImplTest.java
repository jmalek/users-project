package com.jmalek.users.application.impl;

import com.jmalek.users.application.UserEmailNotValidated;
import com.jmalek.users.domain.model.User;
import com.jmalek.users.domain.model.UserEmail;
import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.model.UserIdFactory;
import com.jmalek.users.domain.model.UserName;
import com.jmalek.users.domain.model.UserPassword;
import com.jmalek.users.domain.model.UserRepository;
import com.jmalek.users.domain.model.email.SuccessfulUserRegistrationEvent;
import com.jmalek.users.domain.util.TryResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import static com.jmalek.users.application.impl.EmailProducerMock.SUBJECT;
import static com.jmalek.users.application.impl.EmailProducerMock.TEXT;
import static com.jmalek.users.domain.model.UserEmail.UserEmailFactoryImpl.EMAIL_HAS_INVALID_FORMAT;
import static com.jmalek.users.domain.model.UserEmail.UserEmailFactoryImpl.EMAIL_IS_ALREADY_IN_USE;
import static org.assertj.core.api.Assertions.assertThat;

class RegisterUserServiceImplTest {

  private UserServiceImpl sut;
  private UserRepository userRepository;
  private UserIdFactory userIdFactory;
  private EmailProducerMock emailProducer;
  private PasswordEncoder passwordEncoder;

  @BeforeEach
  void setUp() {
    userRepository = new UserRepositoryMock();
    passwordEncoder = new PasswordEncoderMock();
    userIdFactory = () -> new UserId("userId1");
    emailProducer = new EmailProducerMock();
    sut = new UserServiceImpl(new UserEmail.UserEmailFactoryImpl(userRepository), userRepository, userIdFactory, emailProducer, passwordEncoder);
  }

  @Test
  void registerUser() {
    //given
    //when
    TryResult<User> result = sut.register(new UserName("name1"), new UserPassword("password1", passwordEncoder), new UserEmailNotValidated("aaaa@bbbb.eu"));
    //then
    User user = result.getValue();
    assertThat(user.getUserId()).isEqualTo(userIdFactory.create());
    assertThat(user.getUserName()).isEqualTo(new UserName("name1"));
    assertThat(user.getUserEmail().getValue()).isEqualTo("aaaa@bbbb.eu");
    assertThat(user.getUserPassword()).isEqualTo(new UserPassword("password1", passwordEncoder));
    assertThat(emailProducer.getLast()).isEqualTo(new SuccessfulUserRegistrationEvent(user.getUserId().getValue(), user.getUserEmail().getValue(), SUBJECT, TEXT));
  }


  @Test
  void shouldNotRegisterUserWhenEmailIsAlreadyInUse() {
    //given
    String email = "aa@bb.eu";
    sut.register(new UserName("name1"), new UserPassword("password1", passwordEncoder), new UserEmailNotValidated(email));
    //when
    TryResult<User> user = sut.register(new UserName("name2"), new UserPassword("password2", passwordEncoder), new UserEmailNotValidated(email));
    //then
    assertThat(user.isError()).isTrue();
    assertThat(user.getErrorMessage()).isEqualTo(EMAIL_IS_ALREADY_IN_USE);
  }

  @Test
  void shouldNotRegisterUserWhenWrongEmailFormat() {
    //given
    //when
    TryResult<User> user = sut.register(new UserName("name1"), new UserPassword("password1", passwordEncoder), new UserEmailNotValidated("aaaa@bbbb"));
    //then
    assertThat(user.isError()).isTrue();
    assertThat(user.getErrorMessage()).isEqualTo(EMAIL_HAS_INVALID_FORMAT);
    assertThat(emailProducer.getLast()).isNull();
  }

}
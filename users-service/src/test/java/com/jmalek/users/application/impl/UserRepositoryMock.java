package com.jmalek.users.application.impl;

import com.jmalek.users.domain.model.User;
import com.jmalek.users.domain.model.UserEmail;
import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.model.UserRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.of;

public class UserRepositoryMock implements UserRepository {

  private Map<UserEmail, User> userEmails = new HashMap();
  private Map<UserId, User> userIds = new HashMap();

  @Override
  public boolean checkIfEmailExists(UserEmail userEmail) {
    return userEmails.containsKey(userEmail);
  }

  @Override
  public User save(User user) {
    userEmails.put(user.getUserEmail(), user);
    userIds.put(user.getUserId(), user);
    return user;
  }

  @Override
  public Optional<User> loadUser(UserId userId) {
    User user = userIds.get(userId);
    if (user != null) {
      return of(user);
    }
    return Optional.empty();
  }

  @Override
  public List<User> loadAll() {
    return new ArrayList<>(userIds.values());
  }
}

package com.jmalek.users.application.impl;

import com.jmalek.users.application.UserEmailNotValidated;
import com.jmalek.users.domain.model.User;
import com.jmalek.users.domain.model.UserEmail;
import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.model.UserIdFactory;
import com.jmalek.users.domain.model.UserName;
import com.jmalek.users.domain.model.UserPassword;
import com.jmalek.users.domain.model.UserRepository;
import com.jmalek.users.domain.util.TryResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;

class DeleteUserServiceImplTest {

  private UserServiceImpl sut;
  private UserRepository userRepository;
  private UserIdFactory userIdFactory;
  private EmailProducerMock emailProducer;
  private PasswordEncoder passwordEncoderMock;

  @BeforeEach
  void setUp() {
    passwordEncoderMock = new PasswordEncoderMock();
    userRepository = new UserRepositoryMock();
    userIdFactory = () -> new UserId("userId1");
    emailProducer = new EmailProducerMock();
    sut = new UserServiceImpl(new UserEmail.UserEmailFactoryImpl(userRepository), userRepository, userIdFactory, emailProducer, passwordEncoderMock);
  }

  @Test
  void deleteUser() {
    //given
    User registerUser = registerUser();
    //when
    TryResult<User> result = sut.deleteUsers(registerUser.getUserId());
    //then
    User user = result.getValue();
    assertThat(user.isDeleted()).isTrue();
  }

  private User registerUser() {
    return sut.register(new UserName("name1"), new UserPassword("password1", passwordEncoderMock), new UserEmailNotValidated("aa@bb.eu")).getValue();
  }

  @Test
  void shouldNotDeleteUserForNotExistingUserId() {
    //given
    //when
    TryResult<User> result = sut.deleteUsers(new UserId("notExistingUserId"));
    //then
    assertThat(result.isError()).isTrue();
    assertThat(result.getErrorMessage()).isNotEmpty();
  }
}
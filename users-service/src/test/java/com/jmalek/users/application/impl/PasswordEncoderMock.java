package com.jmalek.users.application.impl;

import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordEncoderMock implements PasswordEncoder {
  @Override
  public String encode(CharSequence charSequence) {
    return String.valueOf(charSequence);
  }

  @Override
  public boolean matches(CharSequence charSequence, String s) {
    return false;
  }
}

package com.jmalek.users.application.impl;

import com.jmalek.users.application.UpdateData;
import com.jmalek.users.application.UserEmailNotValidated;
import com.jmalek.users.application.UserService;
import com.jmalek.users.domain.model.User;
import com.jmalek.users.domain.model.UserEmail;
import com.jmalek.users.domain.model.UserEmailFactory;
import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.model.UserIdFactory;
import com.jmalek.users.domain.model.UserName;
import com.jmalek.users.domain.model.UserPassword;
import com.jmalek.users.domain.model.UserRepository;
import com.jmalek.users.domain.model.email.EmailProducer;
import com.jmalek.users.domain.util.TryResult;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.jmalek.users.domain.util.TryResult.error;
import static com.jmalek.users.domain.util.TryResult.of;

@Service
@Transactional
public class UserServiceImpl implements UserService {

  private UserEmailFactory userEmailFactory;
  private UserRepository userRepository;
  private UserIdFactory userIdFactory;
  private EmailProducer emailProducer;
  private PasswordEncoder passwordEncoder;

  public UserServiceImpl(UserEmailFactory userEmailFactory, UserRepository userRepository, UserIdFactory userIdFactory, EmailProducer emailProducer, PasswordEncoder passwordEncoder) {
    this.userEmailFactory = userEmailFactory;
    this.userRepository = userRepository;
    this.userIdFactory = userIdFactory;
    this.emailProducer = emailProducer;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public TryResult<User> register(UserName userName, UserPassword userPassword, UserEmailNotValidated emailNotValidated) {
    TryResult<UserEmail> userEmailTryResult = userEmailFactory.create(emailNotValidated.getEmail());
    if (userEmailTryResult.isError()) {
      return error(userEmailTryResult.getErrorMessage());
    }
    User user = userRepository
            .save(new User(userIdFactory.create(), userName, userPassword, userEmailTryResult.getValue()));
    emailProducer.sendSuccessfulUserRegistration(user.getUserId().getValue(), user.getUserEmail().getValue());
    return of(user);
  }

  @Override
  public TryResult<User> deleteUsers(UserId userId) {
    Optional<User> userOptional = userRepository.loadUser(userId);
    return userOptional.map(user -> {
      user.delete();
      return of(user);
    }).orElseGet(() -> error("User not found with id: " + userId.getValue()));
  }

  @Override
  public List<User> readAll() {
    return userRepository.loadAll();
  }

  @Override
  public TryResult<User> update(UserId userId, UpdateData updateData) {
    Optional<User> userOptional = userRepository.loadUser(userId);
    return userOptional.map(user -> {
      updateData.getName().ifPresent(name -> user.changeName(new UserName(name)));
      updateData.getPassword().ifPresent(password -> user.changePassword(new UserPassword(password, passwordEncoder)));
      return of(user);
    }).orElseGet(() -> error("User not found with id: " + userId.getValue()));
  }
}

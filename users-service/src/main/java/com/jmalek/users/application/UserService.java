package com.jmalek.users.application;

import com.jmalek.users.domain.model.User;
import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.model.UserName;
import com.jmalek.users.domain.model.UserPassword;
import com.jmalek.users.domain.util.TryResult;

import java.util.List;

public interface UserService {
  TryResult<User> register(UserName userName, UserPassword userPassword, UserEmailNotValidated value);

  TryResult<User> deleteUsers(UserId userId);

  List<User> readAll();

  TryResult<User> update(UserId userId, UpdateData updateData);
}

package com.jmalek.users.application;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Optional;

import static java.util.Optional.ofNullable;

@ToString
@EqualsAndHashCode
public class UpdateData {
  private String name;
  private String password;

  public UpdateData(String name, String password) {
    this.name = name;
    this.password = password;
  }

  public Optional<String> getName() {
    return ofNullable(name);
  }

  public Optional<String> getPassword() {
    return ofNullable(password);
  }
}

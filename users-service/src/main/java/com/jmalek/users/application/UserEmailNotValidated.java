package com.jmalek.users.application;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@Getter
public class UserEmailNotValidated {

  private String email;
  public UserEmailNotValidated(String email) {
    this.email = email;
  }
}

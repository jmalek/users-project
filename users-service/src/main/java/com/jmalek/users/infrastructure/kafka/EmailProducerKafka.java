package com.jmalek.users.infrastructure.kafka;

import com.jmalek.users.domain.model.email.EmailProducer;
import com.jmalek.users.domain.model.email.SuccessfulUserRegistrationEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFutureCallback;


@Slf4j
@Component
@ConditionalOnProperty(prefix = "kafka-topics.user.registration.email", name = "producer")
public class EmailProducerKafka implements EmailProducer {

  private final KafkaTemplate kafkaTemplate;
  private final String kafkaTopic;

  public EmailProducerKafka(KafkaTemplate<String, SuccessfulUserRegistrationEvent> registrationUserKafkaTemplate, @Value("${kafka-topics.user.registration.email.producer}") String kafkaTopic) {
    this.kafkaTemplate = registrationUserKafkaTemplate;
    this.kafkaTopic = kafkaTopic;
  }

  @Override
  public SuccessfulUserRegistrationEvent sendSuccessfulUserRegistration(String userId, String userEmail) {
    SuccessfulUserRegistrationEvent successfulUserRegistrationEvent = new SuccessfulUserRegistrationEvent(userId, userEmail, "User service registration", "Registered user with email: " + userEmail);
    kafkaTemplate.send(new ProducerRecord<>(kafkaTopic, successfulUserRegistrationEvent)).addCallback(sendCallback());
    return successfulUserRegistrationEvent;
  }

  private ListenableFutureCallback sendCallback() {
    return new ListenableFutureCallback() {
      @Override
      public void onFailure(Throwable ex) {
        log.error("Unable to send message for SuccessfulUserRegistrationEvent.", ex);
      }

      @Override
      public void onSuccess(Object result) {
        log.info("Sent SuccessfulUserRegistrationEvent: " + result);
      }
    };
  }
}

package com.jmalek.users.infrastructure.factory;

import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.model.UserIdFactory;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UserIdFactoryImpl implements UserIdFactory {

  @Override
  public UserId create() {
    return new UserId(UUID.randomUUID().toString());
  }
}

package com.jmalek.users.infrastructure.hibernate;

import com.jmalek.users.domain.model.User;
import com.jmalek.users.domain.model.UserEmail;
import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.model.UserRepository;
import com.jmalek.users.domain.model.UserStatus;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.of;

@Repository
public class UserRepositoryImpl implements UserRepository {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public boolean checkIfEmailExists(UserEmail userEmail) {
    TypedQuery<User> query = entityManager.createQuery("" +
                    "select u from User u " +
                    "where u.userEmail = (:userEmail)"
            , User.class);
    query.setParameter("userEmail", userEmail);
    List<User> result = query.getResultList();
    return result.size() > 0;
  }

  @Override
  public User save(User user) {
    entityManager.persist(user);
    return user;
  }

  @Override
  public Optional<User> loadUser(UserId userId) {
    TypedQuery<User> query = entityManager.createQuery("" +
                    "select u from User u " +
                    "where u.userId = (:userId) " +
                    "and u.userStatus = (:userStatus) "
            , User.class);
    query.setParameter("userId", userId);
    query.setParameter("userStatus", UserStatus.ACTIVE);
    List<User> result = query.getResultList();
    return (result.size() != 1) ? Optional.empty() : of(result.get(0));
  }

  @Override
  public List<User> loadAll() {
    TypedQuery<User> query = entityManager.createQuery("" +
                    "select u from User u " +
                    "where u.userStatus = (:userStatus) "
            , User.class);
    query.setParameter("userStatus", UserStatus.ACTIVE);
    return query.getResultList();
  }
}

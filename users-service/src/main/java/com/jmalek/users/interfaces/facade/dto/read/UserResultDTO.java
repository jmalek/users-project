package com.jmalek.users.interfaces.facade.dto.read;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import static com.jmalek.users.domain.Preconditions.checkNotEmpty;

@Getter
@EqualsAndHashCode
@ToString
public class UserResultDTO {

  private String email;
  private String userId;
  private String name;

  public UserResultDTO(String userId, String name, String email) {
    this.email = checkNotEmpty(email);
    this.userId = checkNotEmpty(userId);
    this.name = checkNotEmpty(name);
  }

}

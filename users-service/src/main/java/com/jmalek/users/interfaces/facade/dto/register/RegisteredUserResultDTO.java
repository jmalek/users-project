package com.jmalek.users.interfaces.facade.dto.register;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

import static com.jmalek.users.domain.Preconditions.checkNotNull;


@Getter
@EqualsAndHashCode
@ToString
public class RegisteredUserResultDTO {

  private List<RegisteredUserDTO> users;

  public RegisteredUserResultDTO(List<RegisteredUserDTO> users) {
    this.users = checkNotNull(users);
  }
}

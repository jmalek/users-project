package com.jmalek.users.interfaces.facade.dto.register;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import static com.jmalek.users.domain.Preconditions.checkNotEmpty;
import static com.jmalek.users.domain.Preconditions.checkNotNull;

@Getter
@EqualsAndHashCode
@ToString
public class RegisteredUserDTO {

  private String errorMessage;
  private String email;
  private String userId;
  private int httpStatus;

  private RegisteredUserDTO(String errorMessage, HttpStatus httpStatus) {
    this.errorMessage = checkNotEmpty(errorMessage);
    this.httpStatus = checkNotNull(httpStatus).value();
  }

  public RegisteredUserDTO(String email, String userId) {
    this.email = checkNotEmpty(email);
    this.userId = checkNotEmpty(userId);
    this.httpStatus = HttpStatus.OK.value();
  }

  public static RegisteredUserDTO error(String errorMessage, HttpStatus httpStatus) {
    return new RegisteredUserDTO(errorMessage, httpStatus);
  }

}

package com.jmalek.users.interfaces.facade.dto.update;

import com.jmalek.users.domain.util.TryResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import static com.jmalek.users.domain.util.TryResult.error;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class UpdateUserDTO {

  @ApiModelProperty(required = true)
  private String userId;
  private String name;
  private String password;

  public TryResult<String> validate() {
    if (isBlank(userId)) {
      return error("userId filed is required");
    }
    if (name != null && isBlank(name)) {
      return error("name filed can not be blank");
    }
    if (password != null && isBlank(password)) {
      return error("password can not be blank");
    }
    return TryResult.of("OK");
  }
}

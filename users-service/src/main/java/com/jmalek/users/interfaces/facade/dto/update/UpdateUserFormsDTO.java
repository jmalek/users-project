package com.jmalek.users.interfaces.facade.dto.update;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Getter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class UpdateUserFormsDTO {

  //place for some additional params

  @ApiModelProperty(notes = "List of users to update")
  private List<UpdateUserDTO> updateUsers;

  public UpdateUserFormsDTO(List<UpdateUserDTO> updateUsers) {
    this.updateUsers = updateUsers;
  }
}

package com.jmalek.users.interfaces.facade.dto.update;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

import static com.jmalek.users.domain.Preconditions.checkNotNull;

@Getter
@EqualsAndHashCode
@ToString
public class UpdateUsersResultDTO {

  private List<UpdatedUserDTO> users;

  public UpdateUsersResultDTO(List<UpdatedUserDTO> users) {
    this.users = checkNotNull(users);
  }
}

package com.jmalek.users.interfaces.controller;

import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.delete.DeleteUsersDTO;
import com.jmalek.users.interfaces.facade.dto.delete.DeletedUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.read.ReadUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserFormsDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUserFormsDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUsersResultDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users")
public class UserController {

  private UserFacade userFacade;

  public UserController(UserFacade userFacade) {
    this.userFacade = userFacade;
  }

  @ApiOperation(value = "Register multiple users")
  @ApiResponses({
          @ApiResponse(responseCode = "207", description = "Multi-Status")
  })
  @PostMapping
  public ResponseEntity<RegisteredUserResultDTO> register(@RequestBody RegistrationUserFormsDTO userFormDataDTO) {
    RegisteredUserResultDTO registeredUserDTOs = this.userFacade.register(userFormDataDTO);
    return new ResponseEntity<>(registeredUserDTOs, HttpStatus.MULTI_STATUS);
  }

  @ApiOperation(value = "Delete multiple users")
  @ApiResponses({
          @ApiResponse(responseCode = "207", description = "Multi-Status")
  })
  @DeleteMapping
  public ResponseEntity<DeletedUsersResultDTO> delete(@RequestBody DeleteUsersDTO deleteUsersDTO) {
    DeletedUsersResultDTO deleteUsersResult = this.userFacade.delete(deleteUsersDTO);
    return new ResponseEntity<>(deleteUsersResult, HttpStatus.MULTI_STATUS);
  }

  @ApiOperation(value = "Read users")
  @ApiResponses({
          @ApiResponse(responseCode = "200", description = "OK")
  })
  @GetMapping
  public ResponseEntity<ReadUsersResultDTO> read() {
    ReadUsersResultDTO readUsersResultDTO = this.userFacade.readAll();
    return new ResponseEntity<>(readUsersResultDTO, HttpStatus.OK);
  }

  @ApiOperation(value = "Update users")
  @ApiResponses({
          @ApiResponse(responseCode = "207", description = "Multi-Status")
  })
  @PatchMapping
  public ResponseEntity<UpdateUsersResultDTO> update(@RequestBody UpdateUserFormsDTO updateUserFormsDTO) {
    UpdateUsersResultDTO updateUsersResultDTO = this.userFacade.update(updateUserFormsDTO);
    return new ResponseEntity<>(updateUsersResultDTO, HttpStatus.MULTI_STATUS);
  }

}

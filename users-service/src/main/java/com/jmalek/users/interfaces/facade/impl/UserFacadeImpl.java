package com.jmalek.users.interfaces.facade.impl;

import com.jmalek.users.application.UpdateData;
import com.jmalek.users.application.UserEmailNotValidated;
import com.jmalek.users.application.UserService;
import com.jmalek.users.domain.model.User;
import com.jmalek.users.domain.model.UserId;
import com.jmalek.users.domain.model.UserName;
import com.jmalek.users.domain.model.UserPassword;
import com.jmalek.users.domain.util.TryResult;
import com.jmalek.users.interfaces.facade.UserFacade;
import com.jmalek.users.interfaces.facade.dto.delete.DeleteUsersDTO;
import com.jmalek.users.interfaces.facade.dto.delete.DeletedUserDTO;
import com.jmalek.users.interfaces.facade.dto.delete.DeletedUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.read.ReadUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.read.UserResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserFormsDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUserDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUserFormsDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdatedUserDTO;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Component
public class UserFacadeImpl implements UserFacade {

  private final UserService userService;
  private PasswordEncoder passwordEncoder;

  public UserFacadeImpl(UserService userService, PasswordEncoder passwordEncoder) {
    this.userService = userService;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public ReadUsersResultDTO readAll() {
    List<User> users = userService.readAll();
    return new ReadUsersResultDTO(
            users.stream().map(u -> new UserResultDTO(
                    u.getUserId().getValue(),
                    u.getUserName().getValue(),
                    u.getUserEmail().getValue()))
                    .collect(Collectors.toList())
    );
  }

  @Override
  public UpdateUsersResultDTO update(UpdateUserFormsDTO updateUserFormsDTO) {
    if (updateUserFormsDTO == null || updateUserFormsDTO.getUpdateUsers() == null) {
      return new UpdateUsersResultDTO(emptyList());
    }
    List<UpdatedUserDTO> updatedUsers = new ArrayList<>();
    for (UpdateUserDTO u : updateUserFormsDTO.getUpdateUsers()) {
      TryResult<User> userTryResult = doUpdate(u);
      updatedUsers.add(createUpdatedUserDTO(u.getUserId(), userTryResult));
    }
    return new UpdateUsersResultDTO(updatedUsers);
  }

  private TryResult<User> doUpdate(UpdateUserDTO u) {
    TryResult<String> validate = u.validate();
    if (validate.isError()) {
      return TryResult.error(validate.getErrorMessage());
    } else {
      return userService.update(new UserId(u.getUserId()), new UpdateData(u.getName(), u.getPassword()));
    }
  }

  private UpdatedUserDTO createUpdatedUserDTO(String userId, TryResult<User> result) {
    if (result.isError()) {
      return UpdatedUserDTO.error(result.getErrorMessage(), HttpStatus.BAD_REQUEST, userId);
    } else {
      return new UpdatedUserDTO(result.getValue().getUserId().getValue());
    }
  }

  @Override
  public DeletedUsersResultDTO delete(DeleteUsersDTO deleteUsersDTO) {
    if (deleteUsersDTO == null || deleteUsersDTO.getUserIds() == null) {
      return new DeletedUsersResultDTO(emptyList());
    }
    List<DeletedUserDTO> deletedUsers = new ArrayList<>();
    for (String userId : deleteUsersDTO.getUserIds()) {
      if (isNotBlank(userId)) {
        TryResult<User> result = userService.deleteUsers(new UserId(userId));
        deletedUsers.add(createDeletedUserDTO(userId, result));
      }
    }
    return new DeletedUsersResultDTO(deletedUsers);
  }

  private DeletedUserDTO createDeletedUserDTO(String userId, TryResult<User> result) {
    if (result.isError()) {
      return DeletedUserDTO.error(result.getErrorMessage(), userId, HttpStatus.BAD_REQUEST);
    } else {
      return new DeletedUserDTO(result.getValue().getUserId().getValue());
    }
  }

  @Override
  public RegisteredUserResultDTO register(RegistrationUserFormsDTO userFormDataDTO) {
    if (userFormDataDTO == null || userFormDataDTO.getRegistrationUserForms() == null) {
      return new RegisteredUserResultDTO(emptyList());
    }
    return new RegisteredUserResultDTO(userFormDataDTO.getRegistrationUserForms().stream()
            .map(this::doRegister)
            .map(this::createRegisteredUserResultDTO)
            .collect(Collectors.toList()));
  }

  private TryResult<User> doRegister(RegistrationUserDTO dto) {
    TryResult<String> validate = dto.validate();
    if (validate.isError()) {
      return TryResult.error(validate.getErrorMessage());
    }
    return userService.register(
            new UserName(dto.getName()),
            new UserPassword(dto.getPassword(), passwordEncoder),
            new UserEmailNotValidated(dto.getEmail())
    );
  }

  private RegisteredUserDTO createRegisteredUserResultDTO(TryResult<User> result) {
    if (result.isError()) {
      return RegisteredUserDTO.error(result.getErrorMessage(), HttpStatus.BAD_REQUEST);
    } else {
      User user = result.getValue();
      return new RegisteredUserDTO(
              user.getUserEmail().getValue(),
              user.getUserId().getValue()
      );
    }
  }
}

package com.jmalek.users.interfaces.facade;

import com.jmalek.users.interfaces.facade.dto.delete.DeleteUsersDTO;
import com.jmalek.users.interfaces.facade.dto.delete.DeletedUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.read.ReadUsersResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegisteredUserResultDTO;
import com.jmalek.users.interfaces.facade.dto.register.RegistrationUserFormsDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUserFormsDTO;
import com.jmalek.users.interfaces.facade.dto.update.UpdateUsersResultDTO;

public interface UserFacade {
  RegisteredUserResultDTO register(RegistrationUserFormsDTO userFormDTO);

  DeletedUsersResultDTO delete(DeleteUsersDTO deleteUsersDTO);

  ReadUsersResultDTO readAll();

  UpdateUsersResultDTO update(UpdateUserFormsDTO updateUserFormsDTO);
}

package com.jmalek.users.interfaces.facade.dto.delete;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

import static com.jmalek.users.domain.Preconditions.checkNotNull;

@Getter
@EqualsAndHashCode
@ToString
public class DeletedUsersResultDTO {

  private List<DeletedUserDTO> users;

  public DeletedUsersResultDTO(List<DeletedUserDTO> users) {
    this.users = checkNotNull(users);
  }
}

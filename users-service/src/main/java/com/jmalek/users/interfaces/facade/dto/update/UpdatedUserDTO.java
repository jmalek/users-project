package com.jmalek.users.interfaces.facade.dto.update;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import static com.jmalek.users.domain.Preconditions.checkNotEmpty;
import static com.jmalek.users.domain.Preconditions.checkNotNull;

@Getter
@EqualsAndHashCode
@ToString
public class UpdatedUserDTO {

  private String errorMessage;
  private String userId;
  private int httpStatus;

  private UpdatedUserDTO(String errorMessage, HttpStatus httpStatus, String userId) {
    this.userId = userId;
    this.errorMessage = checkNotEmpty(errorMessage);
    this.httpStatus = checkNotNull(httpStatus).value();
  }

  public UpdatedUserDTO(String userId) {
    this.userId = checkNotEmpty(userId);
    this.httpStatus = HttpStatus.OK.value();
  }

  public static UpdatedUserDTO error(String errorMessage, HttpStatus httpStatus, String userId) {
    return new UpdatedUserDTO(errorMessage, httpStatus, userId);
  }

}

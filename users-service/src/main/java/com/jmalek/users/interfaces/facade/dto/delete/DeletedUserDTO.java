package com.jmalek.users.interfaces.facade.dto.delete;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import static com.jmalek.users.domain.Preconditions.checkNotEmpty;
import static com.jmalek.users.domain.Preconditions.checkNotNull;

@Getter
@EqualsAndHashCode
@ToString
public class DeletedUserDTO {

  private String errorMessage;
  private String userId;
  private int httpStatus;

  private DeletedUserDTO(String errorMessage, String userId, HttpStatus httpStatus) {
    this.errorMessage = checkNotEmpty(errorMessage);
    this.userId = checkNotEmpty(userId);
    this.httpStatus = checkNotNull(httpStatus).value();
  }

  public DeletedUserDTO(String userId) {
    this.userId = checkNotEmpty(userId);
    this.httpStatus = HttpStatus.OK.value();
  }

  public static DeletedUserDTO error(String errorMessage, String userId, HttpStatus httpStatus) {
    return new DeletedUserDTO(errorMessage, userId, httpStatus);
  }

}

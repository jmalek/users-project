package com.jmalek.users.interfaces.facade.dto.register;

import com.jmalek.users.domain.util.TryResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import static com.jmalek.users.domain.util.TryResult.error;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class RegistrationUserDTO {

  @ApiModelProperty(required = true)
  private String name;
  @ApiModelProperty(required = true)
  private String password;
  @ApiModelProperty(required = true)
  private String email;

  public TryResult<String> validate() {
    if (isBlank(name)) {
      return error("name filed is required");
    }
    if (isBlank(password)) {
      return error("password filed is required");
    }
    if (isBlank(email)) {
      return error("email filed is required");
    }
    return TryResult.of("OK");
  }
}


package com.jmalek.users.interfaces.facade.dto.register;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Getter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class RegistrationUserFormsDTO {

  //place for some additional params

  @ApiModelProperty(notes = "List of users to register")
  private List<RegistrationUserDTO> registrationUserForms;

  public RegistrationUserFormsDTO(List<RegistrationUserDTO> registrationUserForms) {
    this.registrationUserForms = registrationUserForms;
  }
}

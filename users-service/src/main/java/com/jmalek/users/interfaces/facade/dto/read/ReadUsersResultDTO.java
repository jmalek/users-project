package com.jmalek.users.interfaces.facade.dto.read;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

import static com.jmalek.users.domain.Preconditions.checkNotNull;

@Getter
@EqualsAndHashCode
@ToString
public class ReadUsersResultDTO {

  private List<UserResultDTO> users;

  public ReadUsersResultDTO(List<UserResultDTO> users) {
    this.users = checkNotNull(users);
  }
}

package com.jmalek.users.interfaces.facade.dto.delete;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Getter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class DeleteUsersDTO {

  @ApiModelProperty(notes = "List of user ids to delete")
  private List<String> userIds;

  public DeleteUsersDTO(List<String> userIds) {
    this.userIds = userIds;
  }
}

package com.jmalek.users.config;

import com.jmalek.users.config.props.KafkaTopicProps;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaTopicConfig {

  @Bean
  @ConfigurationProperties("kafka-topics.user.registration.email")
  public KafkaTopicProps kafkaTopicProps()  {
    return new KafkaTopicProps();
  }

  @Bean
  public NewTopic topic1(KafkaTopicProps kafkaTopicProps) {
    return new NewTopic(kafkaTopicProps.getProducer(), 1, (short) 1);
  }
}

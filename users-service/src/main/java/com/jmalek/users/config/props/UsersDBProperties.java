package com.jmalek.users.config.props;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "users.spring.datasource.hibernate")
public class UsersDBProperties {

  private String dialect;
  private String ddlauto;
  private boolean showsql;

  public UsersDBProperties() {
  }

  public String getDialect() {
    return dialect;
  }

  public String getDdlauto() {
    return ddlauto;
  }

  public boolean isShowsql() {
    return showsql;
  }

  public void setDialect(String dialect) {
    this.dialect = dialect;
  }

  public void setDdlauto(String ddlauto) {
    this.ddlauto = ddlauto;
  }

  public void setShowsql(boolean showsql) {
    this.showsql = showsql;
  }


}

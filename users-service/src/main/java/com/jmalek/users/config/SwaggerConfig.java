package com.jmalek.users.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("users-service")
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.jmalek.users.interfaces.controller"))
            .build();
  }

}

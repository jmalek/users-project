package com.jmalek.users.config;

import com.jmalek.users.config.props.UsersDBProperties;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableJpaRepositories(
        basePackages = "com.jmalek.users.infrastructure.hibernate",
        entityManagerFactoryRef = "userEntityManager",
        transactionManagerRef = "userTransactionManager"
)
public class DatasourceConfig {

  private UsersDBProperties usersDBProperties;

  public DatasourceConfig(UsersDBProperties usersDBProperties) {
    this.usersDBProperties = usersDBProperties;
  }

  @Bean
  @Primary
  @ConfigurationProperties("users.spring.datasource")
  public DataSourceProperties usersDataSourceProperties() {
    return new DataSourceProperties();
  }

  @Bean
  public DataSource usersDataSource() {
    return usersDataSourceProperties()
            .initializeDataSourceBuilder()
            .type(HikariDataSource.class).build();
  }

  @Bean
  LocalContainerEntityManagerFactoryBean usersEntityManager(@Qualifier("usersDataSource") DataSource dataSource) {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(dataSource);
    em.setPackagesToScan("com.jmalek.users.domain.model");
    em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
    Map<String, Object> properties = new HashMap<>();

    if (usersDBProperties != null) {
      if (usersDBProperties.getDdlauto() != null) {
        properties.put("hibernate.hbm2ddl.auto", usersDBProperties.getDdlauto());
      }
      properties.put("hibernate.show_sql", usersDBProperties.isShowsql());
      if (usersDBProperties.getDialect() != null) {
        properties.put("hibernate.dialect", usersDBProperties.getDialect());
      }
    }
    em.setJpaPropertyMap(properties);
    return em;
  }

  @Bean
  PlatformTransactionManager licenseTransactionManager(@Qualifier("usersEntityManager") LocalContainerEntityManagerFactoryBean licenseEntityManager) {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(licenseEntityManager.getObject());
    return transactionManager;
  }
}

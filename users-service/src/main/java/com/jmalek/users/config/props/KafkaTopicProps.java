package com.jmalek.users.config.props;

public class KafkaTopicProps {

  private String producer;

  public KafkaTopicProps() {
  }

  public String getProducer() {
    return producer;
  }

  public void setProducer(String producer) {
    this.producer = producer;
  }
}

package com.jmalek.users.domain.model;

public interface UserIdFactory {

  UserId create();
}

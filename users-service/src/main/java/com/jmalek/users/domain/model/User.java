package com.jmalek.users.domain.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import static com.jmalek.users.domain.Preconditions.checkNotNull;

@ToString
@EqualsAndHashCode
@Entity
@Table(name = "users")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Embedded
  private UserId userId;
  @Embedded
  private UserName userName;
  @Embedded
  private UserPassword userPassword;
  @Embedded
  private UserEmail userEmail;

  @Enumerated(EnumType.STRING)
  @Column(name = "user_status")
  private UserStatus userStatus;

  protected User() {
    //only for jpa
  }

  public User(UserId userId, UserName userName, UserPassword userPassword, UserEmail userEmail) {
    this.userId = checkNotNull(userId);
    this.userName = checkNotNull(userName);
    this.userPassword = checkNotNull(userPassword);
    this.userEmail = checkNotNull(userEmail);
    this.userStatus = UserStatus.ACTIVE;
  }

  public void delete() {
    this.userStatus = UserStatus.DELETED;
  }

  public UserId getUserId() {
    return userId;
  }

  public UserName getUserName() {
    return userName;
  }

  public UserPassword getUserPassword() {
    return userPassword;
  }

  public UserEmail getUserEmail() {
    return userEmail;
  }

  public boolean isDeleted() {
    return this.userStatus.equals(UserStatus.DELETED);
  }

  public void changeName(UserName name) {
    this.userName = name;
  }

  public void changePassword(UserPassword password) {
    this.userPassword = password;
  }
}

package com.jmalek.users.domain.model;

import com.jmalek.users.domain.util.TryResult;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import static com.jmalek.users.domain.util.TryResult.error;
import static com.jmalek.users.domain.util.TryResult.of;

@ToString
@EqualsAndHashCode
@Embeddable
public class UserEmail {

  @Column(name = "email", length = 1000, nullable = false)
  private String value;

  protected UserEmail() {
    //only for jpa
  }

  UserEmail(String email) {
    this.value = email;
  }

  public String getValue() {
    return value;
  }

  @Component
  public static class UserEmailFactoryImpl implements UserEmailFactory {

    public static final String EMAIL_IS_ALREADY_IN_USE = "Email is already in use.";
    public static final String EMAIL_HAS_INVALID_FORMAT = "Email has invalid format.";

    private UserRepository userRepository;

    public UserEmailFactoryImpl(UserRepository userRepository) {
      this.userRepository = userRepository;
    }

    @Override
    public TryResult<UserEmail> create(String email) {
      boolean valid = EmailValidator.getInstance().isValid(email);
      if (!valid) {
        return error(EMAIL_HAS_INVALID_FORMAT);
      }
      UserEmail userEmail = new UserEmail(email);
      boolean exists = userRepository.checkIfEmailExists(userEmail);
      return (exists) ? error(EMAIL_IS_ALREADY_IN_USE) : of(userEmail);
    }
  }

}

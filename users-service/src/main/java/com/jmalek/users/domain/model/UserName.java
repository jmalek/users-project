package com.jmalek.users.domain.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import static com.jmalek.users.domain.Preconditions.checkNotEmpty;

@ToString
@EqualsAndHashCode
@Embeddable
public class UserName {

  @Column(name = "name", length = 1000, nullable = false)
  private String value;

  protected UserName() {
    //only for jpa
  }

  public UserName(String name) {
    this.value = checkNotEmpty(name);
  }

  public String getValue() {
    return value;
  }
}

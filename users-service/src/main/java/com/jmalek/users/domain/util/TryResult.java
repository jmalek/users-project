package com.jmalek.users.domain.util;


import static com.jmalek.users.domain.Preconditions.checkNotEmpty;

public class TryResult<T> {

  private boolean isError;
  private String errorMessage;
  private T value;

  private TryResult() {
    this.value = null;
    this.isError = false;
    this.errorMessage = "";
  }

  public boolean isError() {
    return isError;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public T getValue() {
    return value;
  }

  public static <T> TryResult<T> of(T t) {
    TryResult<T> result = new TryResult<>();
    result.value = t;
    return result;
  }

  public static <T> TryResult<T> error(String errorMessage) {
    TryResult<T> result = new TryResult<>();
    result.isError = true;
    result.errorMessage = checkNotEmpty(errorMessage);
    return result;
  }
}

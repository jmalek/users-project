package com.jmalek.users.domain;

public class Preconditions {

  public static <T> T checkNotNull(T reference) {
    if (reference == null) {
      throw new NullPointerException();
    } else {
      return reference;
    }
  }

  public static String checkNotEmpty(String string) {
    checkNotNull(string);
    if (string.isEmpty()) {
      throw new RuntimeException("Should not be empty.");
    }
    return string;
  }

}

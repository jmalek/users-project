package com.jmalek.users.domain.model;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

  boolean checkIfEmailExists(UserEmail userEmail);

  User save(User user);

  Optional<User> loadUser(UserId userId);

  List<User> loadAll();
}

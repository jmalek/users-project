package com.jmalek.users.domain.model;

public enum UserStatus {
  ACTIVE,
  DELETED
}

package com.jmalek.users.domain.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import static com.jmalek.users.domain.Preconditions.checkNotEmpty;

@ToString
@EqualsAndHashCode
@Embeddable
public class UserId {

  @Column(name = "user_id", length = 1000, nullable = false)
  private String value;

  protected UserId() {
    //only for jpa
  }

  public UserId(String value) {
    this.value = checkNotEmpty(value);
  }

  public String getValue() {
    return value;
  }
}

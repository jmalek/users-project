package com.jmalek.users.domain.model.email;

public interface EmailProducer {

  SuccessfulUserRegistrationEvent sendSuccessfulUserRegistration(String userId, String userEmail);


}

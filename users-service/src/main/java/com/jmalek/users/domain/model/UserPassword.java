package com.jmalek.users.domain.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import static com.jmalek.users.domain.Preconditions.checkNotEmpty;
import static com.jmalek.users.domain.Preconditions.checkNotNull;

@ToString
@EqualsAndHashCode
@Embeddable
public class UserPassword {

  @Column(name = "password", length = 1000, nullable = false)
  private String value;

  protected UserPassword() {
    //only for jpa
  }

  public UserPassword(String password, PasswordEncoder passwordEncoder) {
    checkNotNull(passwordEncoder);
    checkNotEmpty(password);
    this.value = passwordEncoder.encode(password);
  }

  public String getValue() {
    return value;
  }

}

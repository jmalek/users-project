package com.jmalek.users.domain.model;

import com.jmalek.users.domain.util.TryResult;

public interface UserEmailFactory {

  TryResult<UserEmail> create(String email);

}

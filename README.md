# Users project

## Test for Java Developer
```
As a software engineer, I would like to implement microservices based backend APIs to
register/edit/read/(soft) delete multiple users via bulk operations
> Key requirements
    1. The applications should be written in spring-boot, Java or other JVM languages
    2. The applications will be packaged via docker and can run in a multi-container way via
       docker-compose.yml. Please provide details of how to run your application in a
       README file
    3. Upon successful registration, send out a welcome email (you could use fake smtp
       server)
    4. Proper validation, error handling and testing.
    5. Two separate microservices user-service and notification-service
    6. Communication between services can either be synchronous or asynchronous

Additional Requirements
    1. REST API documentation (Compulsory for senior position application)
    2. Partial handling (Optional)

```

## Development stack
Java + TDD + DDD + Spring Boot + Kafka + Docker + Swagger + fake smtp server

## OS
Ubuntu : built and tested on version 20.04.1 LTS

## Prerequisites 
#### The following tools must be installed on your OS
- Java  : built and tested on openjdk version 11.0.2
- Maven : built and tested on version 3.5.4
- Docker: built and tested on version 19.03.13

## Build apps
To build apps run the following command in users-project directory:
```
mvn clean install dockerfile:build
```

## Run apps
To run apps run the following command in users-project directory:
```
docker-compose up
```

## REST API documentation - Swagger
```
http://localhost:8080/swagger-ui/
```

## Read emails
```
http://localhost:1080/api/emails
```